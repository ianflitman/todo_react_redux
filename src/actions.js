const uid = () => Math.random().toString(34).slice(2);

export function addTodo(text, date='today', taglist = 'urgent, code') {
  return {
    type: 'ADD_TODO',
    payload: {
      id: uid(),
      isDone: false,
      text: text,
      due: date,
      tags: taglist.join(", "),
      selected: false
    }
  };
}

export function toggleTodo(id) {
  return {
    type: 'TOGGLE_TODO',
    payload: id
  };
}

export function toggleSelection(id) {
  return {
    type: 'TOGGLE_SELECTION',
    payload: id
  };
}

export function deleteTodo(id) {
  return {
    type: 'REMOVE_TODO',
    payload: id
  };
}

export function setVisibilityFilter(filter){
  return {
    type: 'SET_VISIBILITY_FILTER',
    filter: filter
  };
}

