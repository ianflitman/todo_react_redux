import { List, Map } from 'immutable';

const init = List([
  Map({ id: 0, isDone: true,  text: 'make components', due: '28/10/2016: 21:52', tags: "UI, CSS", selected: false }),
  Map({ id: 1, isDone: false, text: 'design actions', due: '31/10/2016: 14:00',  tags: "Javascript, Module", selected: false}),
  Map({ id: 2, isDone: false, text: 'implement reducer', due: '02/11/2016: 12:10', tags: "Redux, Switch-case", selected: false }),
  Map({ id: 3, isDone: false, text: 'connect components', due: '04/11/2016: 17:00', tags: "Model, Dispatch", selected: false })
]);

const other = List([
    Map({showOption:'all'})
])

export default function reducer(todos=init,  action) {
  switch(action.type) {
    case 'ADD_TODO':
      return todos.push(Map(action.payload));
    case 'TOGGLE_TODO':
      return todos.map(t => {
        if(t.get('id') === action.payload) {
          return t.update('isDone', isDone => !isDone);
        } else {
          return t;
        }
      });
    case 'TOGGLE_SELECTION':
      return todos.map(t => {
        if(t.get('id') === action.payload) {
          return t.update('selected', selected => !selected);
        } else {
          return t;
        }
      });
    case 'REMOVE_TODO':
        //var removal = todos.get('id');
        return todos.filter(todos => todos.get('id') !== action.payload);
          break;

    case 'SET_VISIBILITY_FILTER':
        if(action.filter === 'all'){
          return todos;
        }else if(action.filter === 'done'){
          var allDone = todos.filter(todos => todos.get('isDone') === true);
          return allDone;
        }else if(action.filter === 'active'){
          var allActive = todos.filter(todos => todos.get('isDone') === false);
          return allActive;
        }
          break;

    default:
      return todos;
  }
}

