import { connect } from 'react-redux';
import * as components from './components';
import { addTodo, toggleTodo, toggleSelection, deleteTodo, setVisibilityFilter } from './actions';

export const TodoList = connect(
  function mapStateToProps(state) {
    return { todos: state };
  },
  function mapDispatchToProps(dispatch) {
    return {
      addTodo: (text, dateStr, tags)  => dispatch(addTodo(text, dateStr, tags)),
      toggleTodo: id => dispatch(toggleTodo(id)),
      toggleSelection: id => dispatch(toggleSelection(id)),
      deleteTodo: id => dispatch(deleteTodo(id)),
      setVisibilityFilter: filter => dispatch(setVisibilityFilter(filter))
    };
  }
)(components.TodoList);

