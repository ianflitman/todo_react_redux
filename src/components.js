import React from 'react';
import { DateField } from 'react-date-picker'
import { addTodo, toggleTodo } from './actions';
import ReactDOM from 'react-dom';


export function Maker(props) {
    var dateStr = "";
    var tags = [];
    var taskStr = "";
    var tagStr = "";
    var allTags = "no tags";
    var textarea;

    const createAllTagsMarkup = () => {
        return {__html: allTags};
    };

    const dateChange = (date) => {
        dateStr = date;
    };

    const taskChange = (e) => {
        const task = e.target;
        taskStr = task.value;
    };

    const tagChange = (e) => {
        const tag = e.target;
        tagStr = tag.value;
    };

    const handleTag = (e) => {
        e.preventDefault();
        const tagInput = e.target[0];
        textarea = e.target[2];

        if (tagInput.value == "") {
            alert('Your tag must not be empty!');
            return
        }

        //tags.push(this.tagInput.value); // fails on second use
        tags.push(tagInput.value);
        allTags = 'tags : ' + tags.join(", ");
        textarea.value = allTags;
        console.log(allTags);
        tagInput.value ="";

    };

    const onCreate = (e) => {
        e.preventDefault();
        const input = e.target[0];
        const text = input.value;
        const isLongEnough = text.length > 3;

        if(isLongEnough && dateStr!="" && tags.length > 0){
            props.parentAddTodo(text, dateStr, tags);
            input.value = "";
            allTags =  "no tags";
            textarea.value="no tags";
            //this.tagsAdded.textContent = "no tags";
            //this.due.date = 'Choose Date / Time'
        }else{
            alert('You task must have more than three characters, possess tags, and a date.')
        }
    };
        var dateStyle = {
            margin: '10px',
        };

        return(

        <div className="taskMaker" >
            <div className="title">Create a Task</div>
            <div>
                <form onSubmit={onCreate}>
                    <input className="tagInput"  placeholder="  Enter Task"></input>
                    <button type="submit" className="taskBut">Add Task</button>
                </form>
            </div>
                <div>
                    <form onSubmit={handleTag}>
                        <input className="tagInput" type="text"  placeholder="  Enter Tag"></input>
                        <button type="submit" className="taskBut">Add Tag</button>
                        <textarea className="tagInput" defaultValue="No tags"></textarea>

                    </form>
                <DateField style={dateStyle}
                              dateFormat = "DD/MM/YYYY: HH:mm"
                              placeholder = "Choose Date / Time"
                              onChange={dateChange}
                />
                <br></br>
            </div>
        </div>
        )
}

export function Todo(props) {
    const { todo } = props;

    const selectChange = (e) => {
        props.parentToggleSelection(todo.id);
        //e.preventDefault();
    };
        return(
        <section>
            <div className={todo.isDone ? "leftDone" : "leftActive"}>
                <span>task: {todo.text}</span>
                <br></br>
                <div>due: {todo.due}</div>

                <div>tags: {todo.tags}</div>
            </div>
            <div className={todo.isDone ? "rightDone" : "rightActive"}>
                <input className="checkSelect" type="checkbox" id={todo.id} value="first_checkbox" defaultChecked={todo.selected} onChange={selectChange}/>
            </div>
        </section>)
}

export function TodoList(props) {
  const { todos, toggleTodo, toggleSelection, deleteTodo, addTodo, setVisibilityFilter} = props;

  var selectOption = '';
  var visibilityOption = "done";

  const visibilityHandle = (e) =>{
      //this.setState({visibilityOption: event.target.value});
      visibilityOption = e.target.value;
      console.log(visibilityOption);
      e.stopPropagation()
  };

  const setVisibility = (e) =>{
      var button = e.target;
      console.log(button.value)
      setVisibilityFilter(button.value);
      //e.preventDefault();

  };

  const selectHandle = (e) =>{
      selectOption = e.target.value;
      console.log(selectOption);

      //e.stopPropagation();

  };

  const changeTasks = (e) => {
      switch(selectOption){
          case "markDone":
              todos.map(t => {
                  if (t.get('selected') && !t.get('isDone')){
                      console.log(t.get('selected') + ' is now done');
                      toggleTodo(t.get('id'))
                  }
              });
              break;
          case "markTodo":
              todos.map(t => {
                  if (t.get('selected') && t.get('isDone')){
                      console.log('now needs doing');
                      toggleTodo(t.get('id'))
                  }
              });
              break;
          case "delete":
              todos.map(t => {
                  if (t.get('selected')){
                      console.log('bye bye task');
                      deleteTodo(t.get('id'))
                  }
              });
              break;
      }
      //e.preventDefault();
  };

  return (
    <div className='todo'>
      <Maker parentAddTodo={addTodo} allTags="no tags"/>
      <ul className='todo__list'>
        {todos.map(t => (
          <li key={t.get('id')} className='todo__item'>
            <Todo todo={t.toJS()} parentToggleSelection={toggleSelection}/>
          </li>
        ))}
      </ul>
        <button value='all' onClick={setVisibility} >see all</button>
        <button value='done' onClick={setVisibility}>see done</button>
        <button value='active' onClick={setVisibility}>see active</button>
        <br></br>
        <select className="actionSelect" defaultValue="instructions" onChange={selectHandle}>
            <option value="instructions" disabled>choose option</option>
            <option value="markDone">done</option>
            <option value="markTodo">to do</option>
            <option value="delete">delete</option>
        </select>
        <button className="actionSelect" onClick={changeTasks}>Change selected</button>
    </div>
  );
}


/*
<form>
            <div className="radio">
                <label><input type="radio" value="all" checked={visibilityOption === 'all'} onChange={visibilityHandle}/>show all</label>
            </div>
            <div className="radio">
                <label><input type="radio" value="done" onChange={visibilityHandle} checked={visibilityOption === 'done'}/>show done</label>
            </div>
            <div className="radio">
                <label><input type="radio" value="active" onChange={visibilityHandle} checked={visibilityOption === 'active'}/>show active</label>
            </div>
</form>*/
